from django.contrib import admin

from app_lesson.models import Lesson

admin.site.register(Lesson)
